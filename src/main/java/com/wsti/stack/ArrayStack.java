package com.wsti.stack;

import com.wsti.stack.exception.StackOverflowException;
import com.wsti.stack.exception.StackUnderflowException;

public class ArrayStack<T> implements Stack<T> {

    private static final int DEFAULT_SIZE = 10;

    private int top;
    private int size;
    private Object[] items;

    public ArrayStack() {
        this.top = -1;
        this.size = DEFAULT_SIZE;
        this.items = new Object[DEFAULT_SIZE];
    }

    public ArrayStack(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("Invalid size: " + size);
        }
        this.top = -1;
        this.size = size;
        this.items = new Object[size];
    }

    public boolean push(T item) {
        if ((top + 1) >= size) {
            throw new StackOverflowException();
        }
        items[++top] = item;
        return true;
    }

    public T pop() {
        if (empty()) {
            throw new StackUnderflowException();
        }
        return (T) items[top--];
    }

    public T peek() {
        if(empty()) {
            throw new StackUnderflowException();
        }
        return (T) items[top];
    }

    public boolean empty() {
        return top <= -1;
    }

    public int size() {
        return top + 1;
    }

    public void clear() {
        top = -1;
        items = new Object[size];
    }
}
