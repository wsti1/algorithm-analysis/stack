package com.wsti.stack;

public interface Stack<T> {

    boolean push(T item);

    T pop();

    T peek();

    boolean empty();

    int size();

    void clear();
}
