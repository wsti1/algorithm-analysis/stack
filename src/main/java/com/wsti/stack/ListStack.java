package com.wsti.stack;

import com.wsti.stack.exception.StackUnderflowException;

public class ListStack<T> implements Stack<T> {

    private Node<T> top;
    private int size;

    public ListStack() {
        this.size = 0;
    }

    @Override
    public boolean push(T item) {
        top = new Node<>(top, item);
        size++;
        return true;
    }

    @Override
    public T pop() {
        if (empty()) {
            throw new StackUnderflowException();
        }
        T item = top.item;
        top = top.next;
        size--;
        return item;
    }

    @Override
    public T peek() {
        if (empty()) {
            throw new StackUnderflowException();
        }
        return top.item;
    }

    @Override
    public boolean empty() {
        return top == null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        top = null;
        size = 0;
    }

    private static class Node<T> {

        private Node<T> next;
        private T item;

        public Node(Node<T> next, T item) {
            this.next = next;
            this.item = item;
        }
    }
}
