package com.wsti.stack.exception;

public class StackUnderflowException extends RuntimeException {

    public StackUnderflowException() {
        super("Stack is empty");
    }
}
