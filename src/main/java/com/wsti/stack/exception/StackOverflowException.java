package com.wsti.stack.exception;

public class StackOverflowException extends RuntimeException {

    public StackOverflowException() {
        super("Stack is full");
    }
}
