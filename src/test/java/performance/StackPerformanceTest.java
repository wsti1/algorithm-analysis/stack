package performance;

import com.wsti.stack.ArrayStack;
import com.wsti.stack.ListStack;
import dnl.utils.text.table.TextTable;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class StackPerformanceTest {

    @Test
    public void stackPerformanceTest() {
        List<String[]> finalResult = new ArrayList<>();

        String[][] result1 = invokePerformanceTest(100000);
        String[][] result2 = invokePerformanceTest(1000000);
        String[][] result3 = invokePerformanceTest(10000000);

        Collections.addAll(finalResult, result1);
        Collections.addAll(finalResult, result2);
        Collections.addAll(finalResult, result3);

        finalResult.sort(Comparator.comparing(implementationName -> implementationName[0]));

        String[] columns = {"Implementation", "Item quantity", "Duration"};

        TextTable table = new TextTable(columns, finalResult.toArray(new String[9][]));
        table.printTable();
    }

    public String[][] invokePerformanceTest(int quantityOfItems) {
        String[][] data = new String[3][3];

        Duration duration;
        String quantity = String.valueOf(quantityOfItems);

        duration = arrayStackPerformanceTest(quantityOfItems);
        data[0] = new String[] {"ArrayStack", quantity, String.valueOf(duration.toMillis())};

        duration = listStackPerformanceTest(quantityOfItems);
        data[1] = new String[] {"ListStack", quantity, String.valueOf(duration.toMillis())};

        duration = javaStackPerformanceTest(quantityOfItems);
        data[2] = new String[] {"JavaStack", quantity, String.valueOf(duration.toMillis())};
        return data;
    }

    public Duration arrayStackPerformanceTest(int quantityOfItems) {
        ArrayStack<Integer> stack = new ArrayStack<>(quantityOfItems);

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < quantityOfItems; i++) {
            stack.push(i);
        }
        for (int i = 0; i < quantityOfItems; i++) {
            stack.pop();
        }
        return Duration.between(start, LocalDateTime.now());
    }

    public Duration listStackPerformanceTest(int quantityOfItems) {
        ListStack<Integer> stack = new ListStack<>();

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < quantityOfItems; i++) {
            stack.push(i);
        }
        for (int i = 0; i < quantityOfItems; i++) {
            stack.pop();
        }
        return Duration.between(start, LocalDateTime.now());
    }

    public Duration javaStackPerformanceTest(int quantityOfItems) {
        Stack<Integer> stack = new Stack<>();

        LocalDateTime start = LocalDateTime.now();
        for (int i = 0; i < quantityOfItems; i++) {
            stack.push(i);
        }
        for (int i = 0; i < quantityOfItems; i++) {
            stack.pop();
        }
        return Duration.between(start, LocalDateTime.now());
    }
}
