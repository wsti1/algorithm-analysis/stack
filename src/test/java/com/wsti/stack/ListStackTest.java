package com.wsti.stack;

import com.wsti.stack.exception.StackUnderflowException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ListStackTest {

    private static final String ITEM_1 = "item-1";
    private static final String ITEM_2 = "item-2";
    private static final String ITEM_3 = "item-3";

    private Stack<String> stack;

    @BeforeEach
    public void beforeEach() {
        stack = new ListStack<>();
    }

    @Test
    public void pushTest() {
        stack.push(ITEM_1);
        stack.push(ITEM_2);
        assertThat(stack.peek()).isEqualTo(ITEM_2);
        assertThat(stack.size()).isEqualTo(2);
    }

    @Test
    public void popTest() {
        stack.push(ITEM_1);
        stack.push(ITEM_2);
        stack.pop();
        stack.pop();
        assertThat(stack.size()).isEqualTo(0);
    }

    @Test
    public void popWithUnderflowTest() {
        Executable executable = () -> {
            stack.push(ITEM_1);
            stack.pop();
            stack.pop();
        };
        assertThrows(StackUnderflowException.class, executable,
                "Stack is empty, should throw StackUnderflowException");
    }

    @Test
    public void peekTest() {
        stack.push(ITEM_1);
        assertThat(stack.peek()).isEqualTo(ITEM_1);
        assertThat(stack.size()).isEqualTo(1);
    }

    @Test
    public void peekWithUnderflowTest() {
        Executable executable = stack::peek;
        assertThrows(StackUnderflowException.class, executable,
                "Stack is empty, should throw StackUnderflowException");
    }

    @Test
    public void emptyPositiveTest() {
        assertThat(stack.empty()).isEqualTo(true);
    }

    @Test
    public void emptyNegativeTest() {
        stack.push(ITEM_1);
        assertThat(stack.empty()).isEqualTo(false);
    }

    @Test
    public void sizeTest() {
        stack.push(ITEM_1);
        assertThat(stack.size()).isEqualTo(1);
    }

    @Test
    public void clearAfterNewItemsPoppedTest() {
        stack.push(ITEM_1);
        stack.push(ITEM_2);
        stack.clear();
        assertThat(stack.size()).isEqualTo(0);
    }

    @Test
    public void clearBeforeNewItemsPoppedTest() {
        stack.push(ITEM_1);
        stack.clear();
        stack.push(ITEM_2);
        stack.push(ITEM_3);
        assertThat(stack.peek()).isEqualTo(ITEM_3);
        assertThat(stack.size()).isEqualTo(2);
    }
}
